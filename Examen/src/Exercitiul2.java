import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Exercitiul2 {
    public static void main(String[] args) {
        new GUI();
    }
}

class GUI extends JFrame {

    JTextField jTextField1;
    JTextField jTextField2;
    JButton button = new JButton("Click here");

    GUI() {
        this.setTitle("Exercitiul 2");
        this.setSize(200, 200);
        init();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        jTextField1 = new JTextField();
        jTextField1.setBounds(20, 20, 100, 20);

        button.setBounds(20, 50, 100, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = jTextField1.getText();
                int number = 0;
                String data;

                try {
                    String numeFisier;
                    numeFisier = jTextField1.getText();
                    FileInputStream fileStream = new FileInputStream(numeFisier);
                    InputStreamReader input = new InputStreamReader(fileStream);
                    BufferedReader reader = new BufferedReader(input);

                    while ((data = reader.readLine()) != null) {
                        number += data.length();
                    }

                    jTextField2.setText(number + "");
                } catch (FileNotFoundException ignored) {
                    jTextField2.setText("Fisier invalid");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });

        jTextField2 = new JTextField();
        jTextField2.setBounds(20, 80, 100, 20);

        this.add(jTextField1);
        this.add(jTextField2);
        this.add(button);
    }
}
